#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include <QFileDialog>
#include <QTextEdit>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	QTextEdit *newArea = new QTextEdit();
	ui->tabWidget->addTab(newArea, "New");
	ui->tabWidget->setCurrentIndex(0);
	connect(ui->actionNew, SIGNAL(triggered(bool)), this, SLOT(newTab()));
	connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(openFile()));
	connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(saveFile()));
	connect(ui->actionSave_As, SIGNAL(triggered(bool)), this, SLOT(saveFileAs()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newTab()
{
	QTextEdit *newArea = new QTextEdit();
	char aux[10];
	sprintf(aux, "New %d", tabNumber++);
	QString title = QString::fromStdString(aux);
	ui->tabWidget->addTab(newArea, title);
	ui->tabWidget->setCurrentIndex(ui->tabWidget->currentIndex() + 1);
}

void MainWindow::openFile()
{
	QFile file = QFileDialog::getOpenFileName(this, "Choose File", "C:\\Users\\Raul\\Desktop", tr("*.txt"));
	if (file.open(QIODevice::ReadOnly))
	{		
		QTextEdit *newArea = new QTextEdit();
		newArea->setText(file.readAll());
		file.close();
		QString aux = file.fileName();
		aux.remove(0, aux.lastIndexOf('/')+1);
		ui->tabWidget->addTab(newArea, aux);		
		ui->tabWidget->setCurrentIndex(ui->tabWidget->currentIndex() + 1);
		ui->tabWidget->setTabToolTip(ui->tabWidget->currentIndex(), file.fileName());
	}
}

void MainWindow::saveFile()
{
	QFile file = ui->tabWidget->tabToolTip(ui->tabWidget->currentIndex());
	if (file.open(QIODevice::WriteOnly))
	{
		QTextEdit *aux = dynamic_cast<QTextEdit *>(ui->tabWidget->currentWidget());
		file.write(aux->toPlainText().toLocal8Bit());
		file.close();
	}
	else
		saveFileAs();
}

void MainWindow::saveFileAs()
{
	QFile file = QFileDialog::getSaveFileName(this, "Choose File", "C:\\Users\\Raul\\Desktop", tr("*.txt"));
	if (file.open(QIODevice::WriteOnly))
	{
		QTextEdit *aux = dynamic_cast<QTextEdit *>(ui->tabWidget->currentWidget());
		file.write(aux->toPlainText().toLocal8Bit());
		file.close();
		QString title = file.fileName();
		title.remove(0, title.lastIndexOf('/') + 1);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(), title);
		ui->tabWidget->setTabToolTip(ui->tabWidget->currentIndex(), file.fileName());
	}
}
