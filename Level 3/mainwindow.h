#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
	void newTab();
	void openFile();
	void saveFile();
	void saveFileAs();

private:
    Ui::MainWindow *ui;
	int tabNumber = 1;
};

#endif // MAINWINDOW_H
