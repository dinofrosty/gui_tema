cmake_minimum_required(VERSION 2.8.11)
project(testproject)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5 COMPONENTS Core Widgets REQUIRED)

file(GLOB INCS "*.h")
file(GLOB SRCS "*.cpp")

# Tell CMake to create the helloworld executable
add_executable(helloworld WIN32 ${SRCS} ${INCS})

add_custom_command(
    TARGET helloworld
    PRE_BUILD
    COMMAND uic.exe ${PROJECT_SOURCE_DIR}/mainwindow.ui -o ${PROJECT_SOURCE_DIR}/ui_mainwindow.h
    COMMENT " Running PRE_BUILD action"
    )

# Use the Widgets module from Qt 5.
target_link_libraries(helloworld Qt5::Widgets Qt5::Core)