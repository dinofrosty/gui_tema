#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	ui->centralWidget->setAutoFillBackground(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setBGColor()
{
	QPalette p;
	p.setColor(QPalette::Background, QColor(ui->horizontalSlider->value(),
		ui->horizontalSlider_2->value(), ui->horizontalSlider_3->value()));
	ui->centralWidget->setPalette(p);
}
